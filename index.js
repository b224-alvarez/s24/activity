const getNum = (num) => num;

num = getNum(2);
cube = num ** 3;

console.log(`The cube of ${num} is ${cube}.`);

let address = [258, "Washington Ave NW", "California", 90011];

const [houseNumber, houseStreet, houseState, stateZip] = address;

console.log(
  `I live at ${houseNumber} ${houseStreet}, ${houseState} ${stateZip}`
);

const animal = {
  animalName: "Lolong",
  animalType: "saltwater crocodile",
  animalWeight: 1075,
  animalMeasurementFt: 20,
  animalMeasurementIn: 3,
};

const {
  animalName,
  animalType,
  animalWeight,
  animalMeasurementFt,
  animalMeasurementIn,
} = animal;

function getAnimal() {
  console.log(
    `${animalName} was a ${animalType}. He wieght at ${animalWeight} kgs with a measurement of ${animalMeasurementFt} ft ${animalMeasurementIn} in.`
  );
}

getAnimal(animal);

const numbers = [1, 2, 3, 4, 5, 15];

numbers.forEach((number) => console.log(number));

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);
